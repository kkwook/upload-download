package kr.datasolution.springsample.springsample;

import kr.datasolution.springsample.springsample.Controller.DownloadController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/* @SpringBootApplication = @Configuration + @EnableAutoConfiguration + @ComponentScan
   @Configuration은 설정을 위한 어노테이션으로 개발자가 생성한 class를 Bean으로 생성할 때 한번만 생성
   @Component는 Bean을 생성 할 때 java에서 new로 생성하듯이 생성
   @ComponentScan은 지정한 위치 이하에 있는 @Component(controller, service, repository)와 @Configuration이 붙은 class를 스캔해서 Bean으로 등록
   @EnableAutoConfiguration은 Spring Application Context를 만들 때 자동으로 설정 하는 기능 제공
*/

@SpringBootApplication
public class StarterApplication {
	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}
}
