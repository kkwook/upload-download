package kr.datasolution.springsample.springsample.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/* @RestController = @Controller + @RensponseBody
 @RestController 어노테이션은 모든 메소드들에 대해 @ResponseBody를 적용해주는 역할, @Controller가 붙은 클래스를 bean으로 자등으로 등록
 /hello 요청에 대해서 화면이 아닌, Http Response Body에 "Hello World!" 스트링을 리턴
*/

// Controller는 view 기술을 사용하지만, RestController는 객체를 반환할 때 JSON/XML 타입의 HTTP 응답을 직접 리턴

@RestController
public class HelloController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String helloWorld() { return "Hello World!"; }

    //어노테이션이 @RestController가 아닌 @Controller일 때 view의 요청 경로가 지정됨
    @RequestMapping(value = "/datsol", method = RequestMethod.GET)
    public String datsol() { return "index"; } // thymeleaf 효과
}
