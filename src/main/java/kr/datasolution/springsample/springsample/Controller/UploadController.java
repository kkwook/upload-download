package kr.datasolution.springsample.springsample.Controller;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class UploadController {
    @RequestMapping("/form")
    public String form() {
        return "form";
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile multipartFile) {
        File targetFile = new File("/static/filed/" + multipartFile.getOriginalFilename()); // 실제 파일 이름 가져옴
        try {
            InputStream filestream = multipartFile.getInputStream(); // InputStream 정의
            FileUtils.copyInputStreamToFile(filestream, targetFile); // Inputstream 객체를 File 개겣로 변환
                                                                     //  FileUtils. 일반적인 파일 처리 관련 기능 클래스

        } catch (IOException e) {
            // FileUtils.deleteQuietly(targetFile); // deleteQuietly : 파일, 디렉토리 또는 파일과 하위 디렉토리가있는 디렉토리를 제거
            e.printStackTrace();
        }

        return "redirect:/form";
    }

}
